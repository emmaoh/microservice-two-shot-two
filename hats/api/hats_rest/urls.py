from django.urls import path
from .views import api_hats, api_hat_detail


urlpatterns = [
    path("hats/", api_hats, name="api_hats"),
    path("hats/<int:pk>/", api_hat_detail, name="api_hat_detail"),
]